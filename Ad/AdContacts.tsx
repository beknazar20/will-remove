import React, {useCallback, useEffect, useState} from 'react';
import {useTranslation} from "next-i18next";


import ConfirmContactNumber from '../ConfirmContactNumber';
import { SearchBlockContext } from "../MainHeader";

import {RE_DIGIT_NUMBER, RE_TELEGRAM_USERNAME} from "../../const/regex.const";
import {NUMBER_PLACEHOLDER} from "../../const/phoneNumber.const";

import {Input} from "../UI/Inputs";
import LocationSelectMenu from "../UI/LocationSelectMenu";
import {Switch} from "../UI/Switch";

import {
    Button,
    StyledMessangersWrapper,
    StyledSelectContainer,
    StyledShevronSvgIcon
} from "../ProfilePage/ProflePage.styled";

const AdContacts = (props) => {
    const { t } = useTranslation()

    const {
        currentSelectedLocationItem,
        setCurrentSelectedLocationItem,
        isValidCurrentSelectedLocationItem,
        currentSelectedLocationItemErrors,
        deliverySaved,
        messengerVis,
        setMessengerVis,
        delivery,
        setReadyToDeliver,
        whatsApp,
        setWhatsApp,
        isValidWhatsApp,
        whatsAppErrors,
        telegram,
        setTelegram,
        isValidTelegram,
        telegramErrors,
        setShowError,
        showError,
        contactNumberError,
        resetContactNumberError
    } = props


    const searchBlockRef = React.useRef(null);

    const [isSelectLocationOpen, setIsSelectLocationOpen] = useState(false);

    const resetMessengersInfo = () => {
        setTelegram('@')
        setWhatsApp('+996')
    }

    const onLocationClick = useCallback(
        (e: React.MouseEvent<HTMLInputElement>) => {
            setIsSelectLocationOpen(!isSelectLocationOpen);
        },
        [isSelectLocationOpen]
    );

    const onSelectItemClick = useCallback(
        (value: string | number, name: string) => {
            setCurrentSelectedLocationItem({ id: value, name });
        },
        []
    );

    const onTelegramChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = e.target;
        if (value.length >=33 || !RE_TELEGRAM_USERNAME.test(value)) return;
        setTelegram(value);
        setShowError(false);
    }

    const onWhatsAppChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = e.target;
        if (value.length >=14 || !RE_DIGIT_NUMBER.test(value)) return;
        setWhatsApp(value);
        setShowError(false);
    }


    return (
        <React.Fragment>
            <SearchBlockContext.Provider value={searchBlockRef}>
                <StyledSelectContainer>
                    <Input
                        placeholder={t('choose_city_town_or_region')}
                        error={currentSelectedLocationItemErrors?.[0]}
                        as={Button}
                        label={t('your_location')}
                        onClick={onLocationClick}
                        // onBlur={onSelectBlur}
                        onFocus={() => {
                            setTimeout(() => setIsSelectLocationOpen(true), 100);
                        }}
                        inputRef={searchBlockRef}
                        data-testid="ad-location-btn"
                    >
                        {currentSelectedLocationItem && currentSelectedLocationItem.name}
                        <StyledShevronSvgIcon />
                    </Input>
                    <LocationSelectMenu
                        testId="ad-location-search"
                        open={isSelectLocationOpen}
                        onSelect={onSelectItemClick}
                        closeHandler={() => setIsSelectLocationOpen(false)}
                        withoutSearchHelper={true}
                    />
                </StyledSelectContainer>
            </SearchBlockContext.Provider>
            <Switch
                label={t('delivery_available')}
                onClick={() => setReadyToDeliver(!delivery)}
                value={delivery}
            />
            <ConfirmContactNumber
                contactNumberError={contactNumberError}
                resetContactNumberError={resetContactNumberError}
            />
            <Switch
                label={t('add_messenger')}
                onClick={() => setMessengerVis(!messengerVis)}
                value={messengerVis}
            />
            <StyledMessangersWrapper visible={messengerVis}>
                <Input
                    label={t('whatsApp_number')}
                    placeholder={NUMBER_PLACEHOLDER}
                    value={whatsApp || ''}
                    onChange={onWhatsAppChange}
                    disabled={!messengerVis}
                    error={messengerVis && showError && whatsAppErrors?.[0]}
                    data-testid="ad-whatsapp-number"
                />
                <Input
                    label={t('telegram_user_name')}
                    placeholder='@'
                    value={telegram || ''}
                    onChange={onTelegramChange}
                    disabled={!messengerVis}
                    error={messengerVis && showError && telegramErrors?.[0]}
                    data-testid="ad-telegram-login"
                />
            </StyledMessangersWrapper>
        </React.Fragment>
    );
};

export default AdContacts;
