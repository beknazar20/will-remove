// TODO: переписать под новую архитектуру
import React, { BaseSyntheticEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "next-i18next";

import { RootState} from "../../store";
import { fetchAdTypeDyIdRequest, getInitialAdUuidRequest } from "../../store/products/actions";
import { selectFiltersCategoryIds } from "../../store/temporarySelectors/filters.selectors";
import {
    resetCategoryFilters,
    resetCategoryFiltersById,
    setCategoryFilters,
    setNewCategoryFilters
} from "../../store/actions/filtersActions";
import {
    clearDynamicFieldsOptionsOld, clearDynamicNestedFiltersOld,
    fetchCategoriesRequestOld, fetchDynamicFilterCategoryOld,
    fetchDynamicFiltersRequestOld,
    getOptionalFieldNestedOptionsOld
} from "../../store/actions/categoriesActionsOld";

import useDevice from "../../hooks/useDevice";

import DealTypeFilter from '../Filter/DealTypeFilter'

import { SelectField } from "../UI/SelectWithDropdown";
import { Switch } from "../UI/Switch";

import { getMsFromYear, getYearFromMs } from "../../utils/date.utils";
import { getTypeBusiness } from "../../utils/data.utils";

import { RE_DIGIT } from "../../const/regex.const";
import RenderDynamicFields from "./components/RenderDynamicFields";

import { categoryFormField, categoryFormInitialState, INPUT_DATE_TYPE, INPUT_FLOAT_TYPE } from "./const";
import { transformAdDetailsTypeToFormType } from "./helpers";


const AdCategory = (props) => {
    const dispatch = useDispatch();
    const { t } = useTranslation()
    const { isMobile } = useDevice();
    
    const [dynamicActiveId, setDynamicActiveId] = useState('');
    const [categoryId, setCategoryId] = useState('');
    const [isToggle, setToggle] = useState(true);



    const { categories, dynamicFilters, dynamicFiltersOptions, isLoading:isDynamicFiltersLoading } = useSelector((state: RootState) => state.categoriesOld);
    const currentChosenCategoryFilterIds = useSelector(selectFiltersCategoryIds);
    const dynamicFiltersExist = useMemo(() => dynamicFilters.length > 0, [dynamicFilters])
    const { categoryFilters } = useSelector((state: RootState) => state.filters);


    const {
        adDetails,
        setForm,
        form,
        filtersSaved,
        categoryFiltersSaved,
        multiSelectReadyData,
        multiSelectValues,
        setMultiSelectValues,
        mainRequiredFields,
        setMainRequiredFields,
        adType,
        setRequiredFields,
        requiredFields,
        setCategoryReadyState
    } = props


    useEffect(() => {
        if(adDetails.category?.id && categories.length){
            setForm(transformAdDetailsTypeToFormType(adDetails, categories)); // у adDetails нету subcategory
            dispatch(fetchDynamicFiltersRequestOld(adDetails.category.id))
        } // transform adDetails to form
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [adDetails, categories]);

    useEffect(() => {
        if(form.subcategory.id) {
            dispatch(fetchAdTypeDyIdRequest(form.subcategory.id))
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[form.subcategory.id])

    useEffect(() => {
        setRequiredFields({});
    },[dynamicFilters])

    useEffect(() => {
        if(filtersSaved?.length && dynamicFilters?.length && !isDynamicFiltersLoading) {
            filtersSaved.forEach((currentFilter) => {
                if(currentFilter.filter.nested){
                    dispatch(getOptionalFieldNestedOptionsOld(currentFilter.value))
                }
            })
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [filtersSaved, isDynamicFiltersLoading])


    useEffect(() => {
        dispatch(fetchCategoriesRequestOld());
        dispatch(getInitialAdUuidRequest());
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);



    useEffect(() => {
        if (dynamicActiveId) {
            // dispatch(fetchDynamicFiltersOptionsRequest(dynamicActiveId));
            dispatch(fetchDynamicFilterCategoryOld({optionIds: dynamicActiveId, options: currentChosenCategoryFilterIds}));
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dynamicActiveId, currentChosenCategoryFilterIds]);

    useEffect(() => {
        if(Object.keys(categoryFiltersSaved).length){
            dispatch(setNewCategoryFilters(categoryFiltersSaved));
            setMultiSelectValues(multiSelectReadyData)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[categoryFiltersSaved])

    const handleChange = (key: string, value) => {

        if (value && value.id) {
            dispatch(fetchDynamicFiltersRequestOld(value.id));
        }
        setCategoryId(value.id);

        dispatch(resetCategoryFilters());
        if (key === categoryFormField.CATEGORY) {
            setForm({ ...categoryFormInitialState, category: value });
        } else {
            setForm({ ...form, [key]: value })
        }
        setMainRequiredFields({ ...mainRequiredFields, [key]: { error: null } });
    }

    const handleChangeDeal = (item) => {
        setForm({ ...form, ad_type: {
                ...item,
                title: item.type,
            }})
        setMainRequiredFields({ ...mainRequiredFields, [categoryFormField.TYPE_OF_DEAL]: { error: null } });
    }

    const getDynamicValue = useCallback((filter) => {
        const existsItem = categoryFilters[filter.id];

        if(filter.create_filter_type === INPUT_DATE_TYPE) return getYearFromMs(existsItem?.value);
        if (existsItem) return existsItem.name;
        return '';
    }, [categoryFilters])

    const getDynamicMultipleValue = useCallback((filter) => {
        const currentFilterValues = multiSelectValues[filter.id];

        if(currentFilterValues) return currentFilterValues.value.map(i => i.value);
        return [];
    }, [multiSelectValues])

    const getDynamicInputValue = useCallback((id: string) => {

        const existsItem = categoryFilters[id];
        if (existsItem) return existsItem.value;
        return '';
    }, [categoryFilters])

    const handleMultiSelectChange = useCallback((item, filter?, clearOnChange = false) => {
        // let value = item.id;
        const oldChosenValue = multiSelectValues[filter.id]?.value || [];

        const findedItem = oldChosenValue.findIndex(i => i.id === item.id);

        if(findedItem !== -1) oldChosenValue.splice(findedItem, 1);
        if(findedItem === -1) oldChosenValue.push(item);

        const currentFilter = {
            filter_id: filter.id,
            value: oldChosenValue.slice()
        }

        setMultiSelectValues({
            ...multiSelectValues,
            [filter.id]: currentFilter
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [multiSelectValues]);


    const handleDynamicChange = useCallback((item, filter?, clearOnChange = false, {required} = {required: false}) => {
        let value = item.id;

        if(filter?.create_filter_type === INPUT_DATE_TYPE) value = getMsFromYear(item.value);
        if(filter?.create_filter_type === INPUT_FLOAT_TYPE) value = item.value;

        dispatch(resetCategoryFiltersById(filter.related_filters));
        dispatch(clearDynamicFieldsOptionsOld(filter.related_filters))

        if(clearOnChange) {
            dispatch(clearDynamicNestedFiltersOld())
        } // if it has nested options, clear all filters

        dispatch(setCategoryFilters({
            filter_id: dynamicActiveId,
            value: value,
            filter,
            list_value: [item.value],
            name: item.value,
            option: item.id,
    }));



        if(item.has_options) {
            dispatch(getOptionalFieldNestedOptionsOld(item.id))
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dynamicActiveId]);

    const getOption = useMemo(() => {
        const findedItem = dynamicFiltersOptions.find(i => i.id === dynamicActiveId);
        if (findedItem) return findedItem.options;
        else return [];
    }, [dynamicActiveId, dynamicFiltersOptions]);

    const handleDynamicInputChange = useCallback((e: BaseSyntheticEvent, options = {required: false}) => {
        const {name, value, id} = e.target;

        if (RE_DIGIT.test(value) || value === '') {
            dispatch(
                setCategoryFilters({
                    filter_id: Number(id),
                    value: value,
                    name: name,
                })
            );
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleDynamicNestedClick = (item) => {}

    return (
        <React.Fragment>
            <SelectField
                initValue={{
                    key: 'id',
                    value: adDetails.category
                }}
                fullDropDownOnMobile
                valueKey="name"
                options={categories}
                label={t('category')}
                placeholder={t('category')}
                dropDownTitle={t('choose_category')}
                value={form.category?.name}
                onChange={(item) => handleChange(categoryFormField.CATEGORY, item)}
                oneColumnDropDown={isMobile}
                error={mainRequiredFields[categoryFormField.CATEGORY]?.error}
                markSelected={false}
                testId='ad-category-selector'
            />
            <SelectField
                initValue={{
                    key: 'id',
                    value: adDetails.category
                }}
                fullDropDownOnMobile
                valueKey="name"
                disabled={!form.category?.name}
                options={form.category?.sub_categories}
                label={t('subcategory')}
                placeholder={t('subcategory')}
                dropDownTitle={t('choose_subcategory')}
                value={form.subcategory?.name}
                onChange={(item) => handleChange(categoryFormField.SUB_CATEGORY, item)}
                oneColumnDropDown={true}
                error={mainRequiredFields[categoryFormField.SUB_CATEGORY]?.error}
                testId='ad-sub-category-selector'
            />
            <DealTypeFilter
                fullDropDownOnMobile
                valueKey='code_value'
                type='select'
                disabled={!form.subcategory?.name}
                options={getTypeBusiness()}
                label={t('transaction_type')}
                placeholder={t('transaction_type')}
                dropDownTitle={t('choose_transaction_type')}
                value={t(form.ad_type.title)}
                onChange={(item) => handleChangeDeal(item)}
                error={mainRequiredFields[categoryFormField.TYPE_OF_DEAL]?.error}
                // @ts-ignore
                adType={adType}
                testId='deal-type-selector'
            />
            {dynamicFiltersExist &&
              <Switch
                label={t('more_text')}
                onClick={() => setToggle(!isToggle)}
                value={isToggle}
              />
            }
            {/*@ts-ignore*/}
            <RenderDynamicFields
                dynamicFilters={dynamicFilters}
                dynamicFiltersExist={dynamicFiltersExist}
                isToggle={isToggle}
                requiredFields={requiredFields}
                setRequiredFields={setRequiredFields}
                getDynamicInputValue={getDynamicInputValue}
                handleDynamicInputChange={handleDynamicInputChange}
                setDynamicActiveId={setDynamicActiveId}
                getOption={getOption}
                getDynamicMultipleValue={getDynamicMultipleValue}
                handleMultiSelectChange={handleMultiSelectChange}
                handleDynamicNestedClick={handleDynamicNestedClick}
                getDynamicValue={getDynamicValue}
                handleDynamicChange={handleDynamicChange}
                deps={[
                    dynamicFilters,
                    getDynamicValue,
                    getOption,
                    isToggle,
                    requiredFields,
                    multiSelectValues
                ]}
            />
        </React.Fragment>
    );
};

export default AdCategory;
