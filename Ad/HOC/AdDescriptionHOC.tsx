import React, { useCallback, useImperativeHandle, useMemo, useState } from 'react';
import { useTranslation } from "next-i18next";

import { hasValue } from "@utils/hasValue";
import { checkCorrectSymbols, checkEmoji, checkSeveralCharacter, lengthIsMoreThan } from "@utils/string.utils";

import { IErrorsFormState, TAdDescriptionHOCProps } from "../types";

import AdDescription from "../AdDescription";
import { INPUT_FIELDS_DEFAULT } from '@const/data.const';


const lengthIsMoreThanOne = (value) => lengthIsMoreThan(1, value?.trim());

const AdDescriptionHOC: React.FC<TAdDescriptionHOCProps> = React.forwardRef((props, ref): JSX.Element => {

    const { t } = useTranslation();

    const {
        product,
        children,
    } = props

    const [form, setForm] = useState(product);
    const [errors, setErrors] = useState<IErrorsFormState>({title: null, description: null});
    const [mainPhotoIndex, setMainPhotoIndex] = useState(0);

    const validateMaxMin = useCallback((value, {max = 100}) => {
        if (!lengthIsMoreThanOne(value)) {
            return t('min_n_character', {n:2});
        }
        if (value.trim()?.length > max) {
            return t('max_n_character', {n:max});
        }
        return null;
    },[])

    const validateSymbols = useCallback((value) => {
        if (checkEmoji(value)) {
            return t('do_not_use_emoji');
        }
        if (!checkCorrectSymbols(value)) {
            return t("use_cyrillic_or_latin");
        }
        if (checkSeveralCharacter(value)) {
            return t("do_not_type_several_char");
        }
        return null;
    },[])

    const isValid = useMemo(() => {
        return (
          hasValue(form.title) &&
          hasValue(form.description) &&
          !validateMaxMin(form.title, { max: 50 }) &&
          !validateMaxMin(form.description, { max: 1000 }) &&
          !validateSymbols(form.title) &&
          !validateSymbols(form.description)
        );
    }, [form.title, form.description]);

    const handleSubmit = () => {
        if (!isValid) {
            setErrors({
                title: validateMaxMin(form.title,{max:50}) || validateSymbols(form.title),
                description: validateMaxMin(form.description,{max:1000}) || validateSymbols(form.description),
            })
        }

        const imageList = form.images.length
          ? [form.images[mainPhotoIndex], ...form.images.filter((img, idx) => idx !== mainPhotoIndex)]
          : []

        const data = {
            title: form.title,
            description: form.description,
            images: imageList,
            price: product.price ?? INPUT_FIELDS_DEFAULT.PRICE,
            location: product.location ?? INPUT_FIELDS_DEFAULT.LOCATION,
        }

        return {
            isValid,
            data
        }
    }

    useImperativeHandle(ref, () => ({
        handleSubmit
    }), [handleSubmit])

    return (
        <React.Fragment>
            <AdDescription
                form={form}
                setForm={setForm}
                errors={errors}
                setErrors={setErrors}
                mainPhotoIndex={mainPhotoIndex}
                setMainPhotoIndex={setMainPhotoIndex}
                product={product}
            />
            {children}
        </React.Fragment>
    );
})

AdDescriptionHOC.displayName = 'AdDescriptionHOC'

export default AdDescriptionHOC;
