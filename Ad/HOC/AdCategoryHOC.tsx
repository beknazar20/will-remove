import React, { useImperativeHandle, useState } from 'react';
import { useSelector } from "react-redux";
import { useTranslation } from "next-i18next";

import { RootState } from "../../../store";

import { useDecryptTelegramAndWhatsapp } from "../../../hooks/useDecrypt";
import { useStateWithValidation } from "../../../hooks/useStateWithValidation";

import cleanObject from "../../../utils/cleanObject";

import { categoryFormField, categoryFormInitialState, initialRequiredFields} from "../const";
import {INPUT_FIELDS_DEFAULT} from "../../../const/data.const";

import { TAdCategoryHOCProps } from "../types";

import AdCategory from "../AdCategory";


const AdCategoryHOC: React.FC<TAdCategoryHOCProps> = React.forwardRef((props, ref): JSX.Element => {
    const { t } = useTranslation()

    const {
        product,
        children,
        adType,
        categoryFiltersSaved,
        multiSelectReadyData,
        filtersSaved,
        telegramSaved,
        whatsAppSaved
    } = props

    const { categoryFilters } = useSelector((state: RootState) => state.filters);

    const [form, setForm] = useStateWithValidation(categoryFormInitialState,[],true);

    const [isToggle, setToggle] = useState(true);
    const [requiredFields, setRequiredFields] = useState({});
    const [mainRequiredFields, setMainRequiredFields] = useState(initialRequiredFields);
    const [multiSelectValues, setMultiSelectValues] = useState({});

    const {validSavedTelegram, validSavedWhatsApp} = useDecryptTelegramAndWhatsapp(telegramSaved, whatsAppSaved);

    const checkIsRequiredFieldsFilled = (requiredFields, categoryFilters) => {
        let isValid = true;
        const requiredFieldsIds = Object.keys(requiredFields);

        requiredFieldsIds.forEach((filterID, index) => {
            if(!categoryFilters[filterID]) {
                setRequiredFields(prev=> ({...prev, [filterID]: {error: t('required_box')}}))
                isValid = false;
            } else {
                setRequiredFields(prev=> ({...prev, [filterID]: {error: null}}))
            }
        });

        return  checkIsChosenCategory() && isValid;
    }


    const checkIsChosenCategory = () => {
        if(!form.category?.name || !form.subcategory?.name || !form.ad_type?.title) {
            const errorFields = {
                [categoryFormField.CATEGORY]: { label: categoryFormField.CATEGORY, error: !form.category?.name ? t('required_box') : null },
                [categoryFormField.SUB_CATEGORY]: { label: categoryFormField.SUB_CATEGORY, error: !form.subcategory?.name ? t('required_box') : null },
                [categoryFormField.TYPE_OF_DEAL]: { label: categoryFormField.TYPE_OF_DEAL, error: !form.ad_type?.title ? t('required_box') : null },
            }

            setMainRequiredFields((prev) => ({
                ...prev,
                ...errorFields,
            }));

            return false
        }

        setMainRequiredFields(initialRequiredFields);

        return true;
    }

    const handleSubmit = () => {
        const categoryCopy = {...categoryFilters};

        Object.entries<{ filter_id: number; value: { id: number }[] }>(
            multiSelectValues
        ).forEach(([id, filter]) => {
            categoryCopy[id] = {
                ...filter,
                value: String(filter.value.map((i) => i.id)),
            };
        });

        cleanObject(categoryCopy);
        const arrayFromCategories = Object.values(categoryCopy);

        if(!checkIsRequiredFieldsFilled(requiredFields, categoryFilters) ) { // если не все поля заполнены или не выбрана категория
            if(!isToggle) setToggle(true); // открыть дополнительные поля если не все поля заполнены
            return {
                isValid: false,
                data: {}
            }
        }

        const data = {
            category: form.subcategory.id,
            ad_type: form.ad_type.title,
            price: product?.price ?? INPUT_FIELDS_DEFAULT.PRICE,
            currency: product.currency ?? 'som',
            filters: arrayFromCategories,
            telegram_profile: validSavedTelegram,
            whatsapp_num: validSavedWhatsApp,
        }
        cleanObject(product)

        return {
            isValid: isToggle,
            data
        }
    }

    useImperativeHandle(ref, () => ({
        handleSubmit
    }), [handleSubmit])


    return (
        <React.Fragment>
            <AdCategory
                adDetails={product}
                setForm={setForm}
                form={form}
                setRequiredFields={setRequiredFields}
                requiredFields={requiredFields}
                filtersSaved={filtersSaved}
                categoryFiltersSaved={categoryFiltersSaved}
                multiSelectReadyData={multiSelectReadyData}
                multiSelectValues={multiSelectValues}
                setMultiSelectValues={setMultiSelectValues}
                mainRequiredFields={mainRequiredFields}
                setMainRequiredFields={setMainRequiredFields}
                adType={adType}
            />
            {children}
        </React.Fragment>
    );
})

AdCategoryHOC.displayName = 'AdCategoryHOC'

export default AdCategoryHOC;
