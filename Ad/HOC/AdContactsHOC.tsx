import React, {useEffect, useImperativeHandle, useState} from 'react';
import { useTranslation } from "next-i18next";
import { useDispatch } from "react-redux";

import { initialContactState } from "../const";
import {TAdContactsHOCProps, TCurrentSelectedLocationItemState } from "../types";
import AdContacts from "../AdContacts";

import { fetchLocationsOldRequest, fetchLocationsRequest } from "@store/actions/locationsActions";
import { getInitialAdUuidRequest } from "@store/products/actions";

import { useDecryptTelegramAndWhatsapp } from "@hooks/useDecrypt";
import { useStateWithValidation } from "@hooks/useStateWithValidation";

import { checkForValidKgMobileNumber, checkForValidTelegram } from "@utils/phoneNumber.utils";





const AdContactsHoc: React.FC<TAdContactsHOCProps> = React.forwardRef((props, ref): JSX.Element => {
    const {
        product,
        children,
        location,
        deliverySaved,
        telegramSaved,
        whatsAppSaved,
        contactNumberIsConfirmed
    } = props

    const dispatch = useDispatch()
    const { t } = useTranslation()

    const {validSavedTelegram, validSavedWhatsApp} = useDecryptTelegramAndWhatsapp(telegramSaved, whatsAppSaved);

    const [delivery, setReadyToDeliver] = useStateWithValidation<boolean>(deliverySaved || initialContactState.readyToDeliver, [], true)
    const [showError, setShowError] = useState(false);
    const [contactNumberError, setContactNumberError] = useState('');
    const [messengerVis, setMessengerVis] = useStateWithValidation<boolean>(Boolean(validSavedTelegram || validSavedWhatsApp),[], true)
    const [
        currentSelectedLocationItem,
        setCurrentSelectedLocationItem,
        isValidCurrentSelectedLocationItem,
        currentSelectedLocationItemErrors
    ] =
        useStateWithValidation<TCurrentSelectedLocationItemState>(location || null , [
            [(value) => !!value?.name, t('required_box')]
        ], true);
    const [whatsApp, setWhatsApp, isValidWhatsApp, whatsAppErrors] =
        useStateWithValidation<string>(validSavedWhatsApp || "+996", [
            [(value) => value.length > 0 || value === "", t('fill_in_the_boxes')],
            [(value) => value.length < 14, t('max_n_character', {n: 14})],
            [checkForValidKgMobileNumber, t('invalid_phone_number')],
        ], true)
    const [telegram, setTelegram, isValidTelegram, telegramErrors] =
        useStateWithValidation<string>(`@${validSavedTelegram}`, [
            [checkForValidTelegram, t('fill_in_the_box')]
        ], true)


    useEffect(() => {
        dispatch(fetchLocationsRequest());
        dispatch(getInitialAdUuidRequest());
        dispatch(fetchLocationsOldRequest())
    }, [])

    const checkMessengers = () => {
        if (!isValidWhatsApp && !isValidTelegram) return false;
        if (!isValidTelegram && telegram.length > 1) return false;
        if (!isValidWhatsApp && whatsApp.length > 4) return false;
        return true;
    }

    const resetContactNumberError = () => {
        setContactNumberError('');
    }

    const handleSubmit = () => {
        const correctTelegram = telegram.replace('@', '');
        const correctWhatsApp = isValidWhatsApp ? whatsApp : '';
        const validMessengers = checkMessengers();

        if (!contactNumberIsConfirmed) {
            setContactNumberError(t('confirm_number_by_sms'))
            return {
                data: {},
                isValid: false
            }
        }

        if (messengerVis) {
            if (!validMessengers) {
                setShowError(true);
                return {
                    data: {},
                    isValid: false,
                    messengerVis: messengerVis
                }
            } else {
                const newFormToSubmit = {
                    location: currentSelectedLocationItem.id,
                    delivery,
                    whatsapp_num: correctWhatsApp,
                    telegram_profile: correctTelegram,
                    category: product.category?.id,
                }

                return {
                    isValid: true,
                    data: newFormToSubmit,
                    messengerVis: messengerVis
                }
            }
        } else {
            if (isValidCurrentSelectedLocationItem) {
                const newFormToSubmit = {
                    location: currentSelectedLocationItem,
                    category: product.category?.id,
                    delivery,
                    whatsapp_num: correctWhatsApp,
                    telegram_profile: correctTelegram,
                }

                return {
                    isValid: true,
                    data: newFormToSubmit,
                    messengerVis: messengerVis
                }
            }
        }
    }

    useImperativeHandle(ref, () => ({
        handleSubmit
    }), [handleSubmit])

    return (
        <React.Fragment>
            <AdContacts
                currentSelectedLocationItem={currentSelectedLocationItem}
                currentSelectedLocationItemErrors={currentSelectedLocationItemErrors}
                setCurrentSelectedLocationItem={setCurrentSelectedLocationItem}
                isValidCurrentSelectedLocationItem={isValidCurrentSelectedLocationItem}
                deliverySaved={product.delivery}
                messengerVis={messengerVis}
                setMessengerVis={setMessengerVis}
                delivery={delivery}
                setReadyToDeliver={setReadyToDeliver}
                whatsApp={whatsApp}
                setWhatsApp={setWhatsApp}
                isValidWhatsApp={isValidWhatsApp}
                whatsAppErrors={whatsAppErrors}
                telegram={telegram}
                setTelegram={setTelegram}
                isValidTelegram={isValidTelegram}
                telegramErrors={telegramErrors}
                setShowError={setShowError}
                showError={showError}
                contactNumberError={contactNumberError}
                resetContactNumberError={resetContactNumberError}
            />
            {children}
        </React.Fragment>
    );
})

AdContactsHoc.displayName = 'AdContactsHoc'

export default AdContactsHoc;
