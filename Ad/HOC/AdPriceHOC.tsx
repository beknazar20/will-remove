import React, {useImperativeHandle, useState} from 'react';
import {useTranslation} from "next-i18next";

import {useDecryptTelegramAndWhatsapp} from "../../../hooks/useDecrypt";
import {useStateWithValidation} from "../../../hooks/useStateWithValidation";

import {IPriceFormState, TAdPriceHOCProps, TAdPriceToggleState} from "../types";

import {RE_DIGIT_PRICE} from "../../../const/regex.const";
import {currencyOptions} from "../const";


import AdPrice from "../AdPrice";
import priceFormatted from "../../../utils/priceFormatted";


const AdPriceHoc: React.FC<TAdPriceHOCProps> = React.forwardRef((props, ref): JSX.Element => {
    const {
        product,
        children,
        telegramSaved,
        whatsAppSaved,
        price
    } = props

    const { t } = useTranslation()

    const {validSavedTelegram, validSavedWhatsApp} = useDecryptTelegramAndWhatsapp(telegramSaved, whatsAppSaved);

    const [isToggle, setToggle] = useState<TAdPriceToggleState>({
        price: false,
        oMoney: false,
    });


    const [form, setForm] = useState<IPriceFormState>({
        currency: currencyOptions[0],
        price: '',
    });

    const [currentPrice, setCurrentPrice, isValidPrice, priceErrors] = useStateWithValidation<string>(priceFormatted(price, form.currency) || '', [[value => RE_DIGIT_PRICE.test(value.replaceAll(" ", '')), t('wrong_price')]], true);

    const handleSubmit = () => {
        if(!isValidPrice) {
            return {
                isValid: false,
                data: {}
            }
        }

        const newFormToSubmit = {
            ...form,
            currency: form.currency.valueToSend,
            price: isToggle.price ? null : currentPrice.replaceAll(" ", ''),
            o_money_pay: isToggle.oMoney,
            contract_price: isToggle.price,
            category: product.category?.id,
            telegram_profile: validSavedTelegram,
            whatsapp_num: validSavedWhatsApp,
            // нужно чтобы бек не ругался
        };

        return {
            isValid: isValidPrice,
            data: newFormToSubmit
        }
    }

    useImperativeHandle(ref, () => ({
        handleSubmit
    }), [handleSubmit])


    return (
        <React.Fragment>
            <AdPrice
                adDetails={product}
                price={price}
                isToggle={isToggle}
                setToggle={setToggle}
                currentPrice={currentPrice}
                setCurrentPrice={setCurrentPrice}
                isValidPrice={isValidPrice}
                priceErrors={priceErrors}
                form={form}
                setForm={setForm}
            />
            {children}
        </React.Fragment>
    );
})

AdPriceHoc.displayName = 'AdPriceHoc'

export default AdPriceHoc;