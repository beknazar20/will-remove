import { IContactFormState, TCategoryFormInitialState, TCurrencyOptions } from "../types";

export const INPUT_DATE_TYPE = "input_date";
export const INPUT_FLOAT_TYPE = "input_float";
export const SELECT_MULTIPLE = 'select_multiple';


export const RENDER_DYNAMIC_FIELDS_TYPE = {
    INPUT_FLOAT: 'input_float',
    INPUT_INT: 'input_int',
    SELECT_MULTIPLE: 'select_multiple',
    INPUT_DATE_TYPE: 'input_date'
}


export const currency = {
    SOM: 'som',
    USD: 'usd',
    CONTRACT: 'Contract'
}

export const categoryFormInitialState: TCategoryFormInitialState = {
    category: {
        id: '',
        name: '',
        sub_categories: [],
    },
    subcategory: {
        name: '',
        id: '',
    },
    ad_type: {
        title:''
    },
}

export const categoryFormReadyState = {
    category: {
        id: '',
        name: '',
        sub_categories: [],
    },
    subcategory: {
        name: '',
        id: '',
    },
    ad_type: {
        title:''
    },
    price: '',
    currency: currency.SOM,
    arrayFromCategories: null,
    validSavedTelegram: '',
    validSavedWhatsApp: ''
}

export const categoryFormField = {
    CATEGORY: 'category',
    SUB_CATEGORY: 'subcategory',
    TYPE_OF_DEAL: 'typeOfDeal',
}

export const initialRequiredFields = {
    [categoryFormField.CATEGORY]: { error: null },
    [categoryFormField.SUB_CATEGORY]: { error: null },
    [categoryFormField.TYPE_OF_DEAL]: { error: null },
};
export const contactFormField: Record<string, string> = {
    LOCATIONS: 'locations',
    SUB_LOCATIONS: 'subLocations',
    READY_DELIVER: 'readyToDeliver',
    PHONE_NUMBER: 'phoneNumber',
    MESSENGER_VIS: 'messengerVis',
    WHATS_APP: 'whatsApp',
    TELEGRAM: 'telegram'
}

export const initialContactState: IContactFormState = {
    locations: {
        name: '',
        sub_locations: [],
    },
    subLocations: {
        name: ''
    },
    readyToDeliver: false,
    phoneNumber: '',
    messengerVis: false,
    whatsApp: '',
    telegram: ''
}


export const currencyOptions: TCurrencyOptions = [
    {
        value: 'KGS',
        label: 'Сом',
        valueToSend:'som'
    },
    {
        value: 'USD',
        label: 'USD',
        valueToSend:'usd'
    },
]