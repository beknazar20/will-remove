import {Dispatch, ForwardedRef, SetStateAction} from "react";

export interface IAdStepsRefProps {
    handleSubmit: () => IAdStepsRefReturnType
}

export type IAdStepsRefReturnType = {
    isValid: boolean,
    data: any,
    messengerVis?: boolean
}

export type TAdDescriptionHOCProps = {
    ref: ForwardedRef<unknown>;
    product: IAdDetails;
}

export type TAdCategoryHOCProps = {
    ref: ForwardedRef<unknown>;
    product: IAdDetails;
    adType: Array<string>
    categoryFiltersSaved: Record<string | number, TAdDetailsFilters>
    multiSelectReadyData,
    filtersSaved: Array<TAdDetailsFilters>;
    telegramSaved: string;
    whatsAppSaved: string
}

export type TAdPriceHOCProps = {
    ref: ForwardedRef<unknown>;
    product: IAdDetails;
    telegramSaved: string;
    whatsAppSaved: string;
    price: string;
}


export type TAdContactsHOCProps = {
    ref: ForwardedRef<unknown>;
    product: IAdDetails;
    location: IAdDetailsLocation;
    deliverySaved: boolean;
    telegramSaved: string
    whatsAppSaved: string;
    contactNumberIsConfirmed: boolean;
}

export type IContactFormState = {
    locations: {
        name: string
        sub_locations: Array<any>
    }
    subLocations: {
        name: string
    }
    readyToDeliver: boolean,
    phoneNumber: string,
    messengerVis: boolean,
    whatsApp: string,
    telegram: string
}

export type TCurrentSelectedLocationItemState = {
    id: string | number;
    name: string
}

export type IPriceFormState = {
    currency: Record<string, string>
    price: string
}

export type TAdPriceToggleState = {
    price: boolean,
    oMoney: boolean,
}

export type IErrorsFormState = {
    title: string;
    description: string;
}

export type TCategoryFormInitialState = {
    category: {
        id: string,
        name: string,
        sub_categories: Array<any>,
    },
    subcategory: {
        name: string,
        id: string,
    },
    ad_type: {
        title: string
    },
}

export type TInitialRequiredFields = {
    [key: string]: TInitialRequiredFieldsError
}

export type TInitialRequiredFieldsError = {
    error: null
}

export type TCategoryFormField = {
    [ECategoryFormField.CATEGORY]: string;
    [ECategoryFormField.SUB_CATEGORY]: string;
    [ECategoryFormField.TYPE_OF_DEAL]: string;
}

export type TCurrencyOptions = Array<TCurrencyOption>

export type TCurrencyOption = {
    value: 'KGS' | 'USD',
    label: 'Сом' | 'USD',
    valueToSend: 'som' | 'usd'
}
export enum ECategoryFormField {
    CATEGORY = 'category',
    SUB_CATEGORY = 'subcategory',
    TYPE_OF_DEAL = 'typeOfDeal',
}








export type TRenderDynamicFieldsProps = {
    dynamicFilters: IDynamicFilters;
    dynamicFiltersExist
    isToggle: boolean;
    requiredFields: Record<number | string, {
        label: string;
        error: null | any;
    }>
    setRequiredFields: Dispatch<SetStateAction<any>>;
    getDynamicInputValue
    handleDynamicInputChange
    setDynamicActiveId
    getOption: Array<any>
    getDynamicMultipleValue
    handleMultiSelectChange
    handleDynamicNestedClick
    getDynamicValue
    handleDynamicChange
    deps: Array<any>;
}











// AD DETAILS TYPES

export interface IAdDetails {
    ad_type: string;
    address: string;
    author: IAdDetailsAuthor;
    author_id: number;
    category: IAdDetailsCategory;
    commentary: string;
    complaint_count: number;
    contact_number: string;
    contract_price: boolean;
    created_at: string;
    currency: string;
    currency_usd: number;
    delivery: boolean;
    description: string;
    detail: number;
    downloadLink: string;
    downloadText: string;
    edit_count: number;
    favorite: boolean;
    filters: TAdDetailsFilterList;
    has_complained: boolean;
    has_image: boolean;
    id: number;
    images: Array<string>;
    is_own: boolean;
    latitude: number | null;
    location: IAdDetailsLocation;
    longitude: number | null;
    minify_images: Array<string>;
    moderator_id: number;
    modified_at: string;
    num_of_views_in_feed: number;
    o_money_pay: boolean;
    old_price: string;
    opening_at: string;
    price: string;
    price_sort: string
    promotion_type: {
        id: number,
        type: EAdDetailsPromotionType,
        description: string,
        img: string
    }; // Q/A
    published_at: string; // Q/A - DATE
    removed_at: string; // Q/A - DATE
    review_count: number;
    status: EAdDetailsStatus;
    telegram_profile: string;
    telegram_profile_is_ident: boolean;
    title: string;
    uuid: string;
    whatsapp_num: string;
    whatsapp_num_is_ident: boolean
}

export interface IAdDetailsAuthor {
    avatar: string;
    block_type: string;
    contact_num_is_ident: boolean;
    contact_number: string;
    has_promotion: boolean;
    id: number;
    location: IAdDetailsAuthorLocation;
    rating: number;
    username: string;
    verified: boolean;
}

export interface IAdDetailsAuthorLocation {
    id: number;
    name: string;
    location_type: EAdDetailsAuthorLocationType;
    parent: number;
    is_default: boolean;
    is_popular: boolean;
    is_recommended: boolean;
    latitude: number;
    longitude: number;
    order_num: number;
    search_by_name: string;
}

export interface IAdDetailsCategory {
    ad_type: Array<number>;
    category_type: string;
    dark_icon: any;
    dark_icon_img: string;
    delivery: boolean;
    filters: Array<number>;
    has_dynamic_filter: any;
    has_map: boolean;
    icon: any;
    icon_img: string;
    id: number;
    is_popular: boolean;
    linked_category: Array<number>;
    name: string;
    order_num: number;
    parent: TAdDetailsParentCategory;
    parent_filters: boolean;
    required_price: boolean;
    transliterate: string;

}

export type TAdDetailsParentCategory = Omit<IAdDetailsCategory, 'parent'> & {
    parent: null;
}


export type TAdDetailsFilterList = Array<TAdDetailsFilters>

export interface TAdDetailsFilters {
    id: number;
    value: string;
    filter_id: number;
    list_value: string | Array<string>;
    filter: TAdDetailsFilter;
}

export interface TAdDetailsFilter {
    create_filter_type: string;
    create_label: string;
    create_title_label: string;
    format: Array<string>;
    id: number;
    list_filter_type: string;
    list_label: string;
    nested: boolean;
    parent_create_label: string
    parent_create_title_label: string;
    parent_list_label: string;
    required: boolean;
    show_breadcrumbs: boolean;
    splitting_popular: boolean;
}

export interface IAdDetailsLocation {
    id: number;
    is_default: boolean;
    is_popular: boolean;
    is_recommended: boolean;
    latitude: number;
    location_type: string;
    longitude: number;
    name: string;
    order_num: number;
    parent: number;
    search_by_name: string;
}


export enum EAdDetailsStatus {
    ACTIVE = 'active',
    MODERATE = 'moderate',
    DISABLED = 'disabled',
    BLOCKED = 'blocked',
    DELETED = 'deleted',
    INITIAL = 'initial',
    CANCELLED = 'cancelled',
    DRAFT = 'draft'
}

export enum EAdDetailsPromotionType {
    VIP = 'VIP',
    UP = 'UP',
    UP_TO_TOP = 'UP_TO_TOP',
    BUSINESS = 'BUSINESS'
}

export enum EAdDetailsAuthorLocationType {
    COUNTRY = 'country',
    REGION = 'region',
    DISTRICT = 'district',
    TOWN = 'town'
}





// FILTERS




export type IDynamicFilters = Array<any>

export interface IDynamicFilter {
    create_filter_type: string;
    create_label: string;
    create_title_label: string;
    filter_fields: string;
    format: Array<string>;
    has_options: boolean;
    id: number;
    list_filter_type: string;
    list_label: string;
    maxLength: null;
    max_range: null | number;
    min_range: null | number;
    parent: null | number ;
    parent_create_label: string | null;
    parent_create_title_label: null | string;
    parent_list_label: null | string;
    position: number;
    related_filters: Array<number>;
    required: boolean;
    show_breadcrumbs: boolean;
    similar_ads_filter: boolean;
    splitting_popular: boolean;
    step: null | number;
    nested?: boolean
}
