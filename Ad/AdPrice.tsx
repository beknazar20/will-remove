import React, { BaseSyntheticEvent, useEffect } from 'react';
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import { getInitialAdUuidRequest } from "../../store/products/actions";
import { selectLanguageAction } from "../../store/app/actions";


import { RE_DIGIT } from "../../const/regex.const";

import { Switch } from "../UI/Switch";
import { SelectField } from "../UI/SelectWithDropdown";
import { Input } from "../UI/Inputs";
import HintBoxOMoney from "../UI/OMoneyHintBox";

import priceFormatted from '../../../app/utils/priceFormatted'
import { currencyOptions } from "./const";


const AdPrice = (props) => {
    const { t } = useTranslation()
    const dispatch = useDispatch();
    const router = useRouter();

    const {
        adDetails,
        isToggle,
        setToggle,
        currentPrice,
        setCurrentPrice,
        priceErrors,
        form,
        setForm
    } = props

    useEffect(() => {
        dispatch(getInitialAdUuidRequest());
        dispatch(selectLanguageAction(router?.locale))
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (adDetails) {
            setForm({
                ...form,
                currency: currencyOptions.find((item) => item.valueToSend === adDetails.currency) || currencyOptions[0],
            });
            setToggle({
                ...isToggle,
                price: adDetails.contract_price,
                oMoney: adDetails.o_money_pay,
            });
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [adDetails]);

    const onChangePrice = (e: BaseSyntheticEvent) => {
        const value = e.target.value.replaceAll(' ', '')
        if(!RE_DIGIT.test(value) && value !== "") return;
        const formattedPrice = priceFormatted(value, form.currency)
        setCurrentPrice(formattedPrice)
    }


    return (
        <React.Fragment>
            <Switch
                margin="-5px 0 24px"
                label={t('negotiable_price_only')}
                value={isToggle.price}
                onClick={() => setToggle({ ...isToggle, price: !isToggle.price })}
            />
            {
                !isToggle.price &&
              <>
                <SelectField
                  oneColumnDropDown
                  valueKey="value"
                  options={currencyOptions}
                  value={form.currency.label}
                  onChange={(item) => setForm({ ...form, currency: item })}
                  label={t('currency')}
                  placeholder={t('currency')}
                  testId='currency-selector'
                />
                <Input
                  onChange={onChangePrice}
                  value={currentPrice}
                  placeholder={t('price')}
                  label={t('price')}
                  error={priceErrors?.[0]}
                  maxLength={11}
                  testId='ad-price-field'
                />
              </>
            }
            <Switch
                label={t('payment_via_O_dengi_acceptable')}
                value={isToggle.oMoney}
                onClick={() => setToggle({ ...isToggle, oMoney: !isToggle.oMoney })}
            />
            <HintBoxOMoney />

        </React.Fragment>
    );
};

export default AdPrice;