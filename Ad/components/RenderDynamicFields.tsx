import React, {useMemo } from 'react';
import { useTranslation } from "next-i18next";

import {INPUT_DATE_TYPE, RENDER_DYNAMIC_FIELDS_TYPE, SELECT_MULTIPLE} from "../const";

import { TRenderDynamicFieldsProps } from "../types";

import { SelectField } from "../../UI/SelectWithDropdown";
import { Input } from "../../UI/Inputs";


const RenderDynamicFields = (props: TRenderDynamicFieldsProps) => {
    const { t } = useTranslation()

    const {
        dynamicFilters,
        dynamicFiltersExist,
        isToggle,
        requiredFields,
        setRequiredFields,
        getDynamicInputValue,
        handleDynamicInputChange,
        setDynamicActiveId,
        getOption,
        getDynamicMultipleValue,
        handleMultiSelectChange,
        handleDynamicNestedClick,
        getDynamicValue,
        handleDynamicChange,
        deps
    } = props


    return useMemo(() => {
        return dynamicFiltersExist && isToggle && dynamicFilters.map((filter, index) => {
            // при создании фильтра в админке, если поле обязательное, то в нем должно быть указано required: true
            if(filter.required && !requiredFields[filter.id]) setRequiredFields(prev => ({...prev, [filter.id]: {label:filter.create_label, error: null}}));
            if (filter.nested && !requiredFields[filter?.nested?.id])
                setRequiredFields((prev) => ({
                    ...prev,
                    [filter?.nested?.id]: {
                        label: filter?.nested?.create_label,
                        error: null,
                    },
                }));

            switch(filter.create_filter_type){
                case RENDER_DYNAMIC_FIELDS_TYPE.INPUT_DATE_TYPE && !filter.has_options:
                case RENDER_DYNAMIC_FIELDS_TYPE.INPUT_FLOAT && !filter.has_options:
                case RENDER_DYNAMIC_FIELDS_TYPE.INPUT_INT && !filter.has_options:
                    return (
                        <Input
                            name={filter.create_label}
                            key={index + filter.id}
                            id={filter.id}
                            label={filter.create_label}
                            placeholder={filter.create_label}
                            value={getDynamicInputValue(filter.id)}
                            onChange={handleDynamicInputChange}
                            onFieldClick={() => setDynamicActiveId(filter.id)}
                        />
                    )
                case RENDER_DYNAMIC_FIELDS_TYPE.SELECT_MULTIPLE:
                    return (
                        <SelectField
                            fullDropDownOnMobile
                            key={index + filter.id}
                            id={filter.id}
                            valueKey="value"
                            options={getOption}
                            label={filter.create_label}
                            error={requiredFields[filter.id]?.error}
                            placeholder={filter.create_label}
                            dropDownTitle="Дополнительно"
                            value={getDynamicMultipleValue(filter)}
                            onChange={(item) =>
                                handleMultiSelectChange(item, filter)
                            }
                            multiple
                            oneColumnDropDown={true}
                            onFieldClick={(id) => {
                                setDynamicActiveId(id);
                                handleDynamicNestedClick(filter);
                            }}
                        />
                    );
                case RENDER_DYNAMIC_FIELDS_TYPE.INPUT_INT:
                    return (
                        <Input
                            name={filter.create_label}
                            key={index + filter.id}
                            id={filter.id}
                            label={filter.create_label}
                            placeholder={filter.create_label}
                            value={getDynamicInputValue(filter.id)}
                            error={requiredFields[filter.id]?.error}
                            onChange={handleDynamicInputChange}
                            onFieldClick={() => setDynamicActiveId(filter.id)}
                        />
                    )
                default:
                    return (
                        <>
                            <SelectField
                                fullDropDownOnMobile
                                key={index + filter.id}
                                id={filter.id}
                                valueKey="value"
                                options={getOption}
                                label={filter.create_label}
                                error={requiredFields[filter.id]?.error}
                                placeholder={filter.create_label}
                                dropDownTitle={t('more_text')}
                                value={getDynamicValue(filter)}
                                onChange={(item) => handleDynamicChange(item, filter, filter.nested)}
                                oneColumnDropDown={true}
                                onFieldClick={id => {
                                    setDynamicActiveId(id);
                                    handleDynamicNestedClick(filter);
                                }}
                            />
                            {
                                filter.nested && (
                                    <SelectField
                                        fullDropDownOnMobile
                                        key={index + filter.nested.id}
                                        id={filter.nested.id}
                                        valueKey="value"
                                        // options={getNestedOption(dynamicFilter)}
                                        options={filter.nested?.options[0]?.options || []}
                                        label={filter.nested.create_label}
                                        error={requiredFields[filter?.nested?.id]?.error}
                                        placeholder={filter.nested.create_label}
                                        dropDownTitle={t('more_text')}
                                        value={getDynamicValue(filter.nested)}
                                        onChange={(item) => handleDynamicChange(item, filter.nested)}
                                        oneColumnDropDown={true}
                                        onFieldClick={id => {
                                            setDynamicActiveId(id);
                                            handleDynamicNestedClick(filter);
                                        }}
                                    />
                                )
                            }
                        </>
                    )

            }})
    }, [...deps])
};

export default RenderDynamicFields;