import styled from "styled-components";
import { device } from "../../../styles/mediaQuery";
import { theme } from "../../../styles/ThemeConfig";

export const StylesEditAd = {
    Page: styled.div`
      width: 100%;
      height: 100%;
      min-height: calc(100vh - 266px);
      background-color: white;
      padding-bottom: 150px;
    `,
    Content: styled.div`
        margin-top: 34px;
        padding: 0 16px;
        @media (${device.tablet}) {
            display: flex;
        }
    `,
    Button: styled.button`
    `,
    Children: styled.div`
        width: 100%;
        height: 100%;
      
        //display: flex;
        //justify-content: center;
        @media (${device.tablet}) {
            //max-width: ${props => props.previewPage ? '100%' : 'calc(100% - 300px)'} ;
        }
      
    `,
    Items: styled.ul`
        display: none;
        @media (${device.tablet}) {
            display: block;
           margin-right: 21px;
        }

    `,
    Item: styled.li`
        font-family: 'Roboto', sans-serif;
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 20px;
        display: flex;
        align-items: center;

        /* GrayScale / Black_3 */

        color: #1C1C1E;
        display: flex;
        align-items: center;
        width: 100%;
        min-width: 245px;
        height: 32px;
        border-radius: 8px;
        padding-left: 12px;
        margin-bottom: 8px;
        cursor: pointer;
        background: ${props => props.active ? theme.gray5 : 'transparent'};
        & > label {
            background-color: transparent;
        }
    `,
    Title: styled.h1`
        text-align: center;
        font-family: 'Roboto', sans-serif;
        font-style: normal;
        font-weight: 500;
        font-size: 24px;
        line-height: 40px;
        color: ${theme.black4};
    `,

    BackButton: styled.button`
        position: absolute;
        left: 20px;
        margin-top: 10px;
    `,
    Wrapper: styled.div`
        max-width: 1140px;
        margin: 0 auto;
        position: relative;
    `,
    MainHeader: styled.div`
        ${({ hide }) => hide && `display: none`};
        box-shadow: 8px 0px 32px rgba(0, 0, 0, 0.08);
        @media (${device.tablet}) {
           display: block;
        }
    `,
    PageHeader: styled.div`
        padding-top: 20px;
        display: flex;
        justify-content: center;
        margin-left: 0;
        @media (${device.tablet}) {
            margin-left: 245px;
        }
     `,
}


export const AdEditForm = styled.form`
  > div:last-child {

    @media(${device.tabletMaxWidth}) {
      margin-bottom: 16px;
    }
    
    > button {
      @media(${device.tabletMaxWidth}) {
        max-width: 169px;
      }

      @media(${device.mobileMaxWidth}) {
        max-width: 100%;
      }
    }
  }

  > div:first-child {
    
    @media(${device.tablet}) {
      width: fit-content;
      min-width: 540px;
      margin: 0 auto;
      max-width: 100%;
    }

    @media(${device.tabletMaxWidth}) {
      width: fit-content;
      min-width: 540px;
      margin: 0 auto;
      max-width: 100%;
    }
    
    
    @media(max-width: 556px) {
      min-width: 100%;
    }

    > div:last-child {
      margin: 5px 0 0 0;
    }
  }

  > div:nth-child(2) {
    margin-top: 44px;
  }

  > div:not(:first-child) {
    @media(${device.tablet}) {
      max-width: 540px;
      margin-left: auto;
      margin-right: auto;
    }

    @media(${device.tabletMaxWidth}) {
      max-width: 540px;
      margin-left: auto;
      margin-right: auto;
    }



    @media(${device.mobileMaxWidth}) {
      max-width: unset;
      margin-left: unset;
      margin-right: unset;
    }
  }
`
