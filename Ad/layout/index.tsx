import React from 'react';
import { useRouter } from "next/router";

import MainHeader from '../../MainHeader';
import { BackLinkHeader } from "../../UI/BackLinkHeader";

import { StylesEditAd } from './styles';
import Footer from "@components/Footer";

interface Props {
    children: React.ReactNode;
    backLink?: string;
    hideMainHeaderOnMobile?: boolean;
}
export const EditAdLayout: React.FC<Props> = ({ children, backLink, hideMainHeaderOnMobile }) => {

    const { back } = useRouter()


    return (
        <>
            <StylesEditAd.Page>
                <StylesEditAd.MainHeader hide={hideMainHeaderOnMobile}>
                    <MainHeader/>
                </StylesEditAd.MainHeader>
                <StylesEditAd.Wrapper>
                    <BackLinkHeader edit={true} title={backLink} handleBack={back}/>
                    <StylesEditAd.Content>
                        <StylesEditAd.Children>
                            {children}
                        </StylesEditAd.Children>
                    </StylesEditAd.Content>
                </StylesEditAd.Wrapper>
            </StylesEditAd.Page>
            <Footer />
        </>
    )
}
