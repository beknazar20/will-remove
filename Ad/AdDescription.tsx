import React, { ChangeEvent, useCallback, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useTranslation } from "next-i18next";

import { buildRemoveImageApiRoute } from "@services/ImageUploader";
import { Api } from "@services/Api";

import { getInitialAdUuidRequest } from "@store/products/actions";

import { getLocalState, localstorageKeys, setlocalState } from "@utils/localStorage";

import { adPageRedirectInitialState } from "@const/redirect.const";


import ImageUploader from "../UI/ImageUploader";
import { Input, Textarea } from "../UI/Inputs";
import { IErrorsFormState } from "./types";

const AdDescription = ({ form, setForm, errors, setErrors, mainPhotoIndex, setMainPhotoIndex, product }) => {
    const dispatch = useDispatch();
    const { t } = useTranslation();


    useEffect(() => {
        dispatch(getInitialAdUuidRequest());
        const localState = getLocalState(localstorageKeys.adPageRedirectValidate)

        if(!localState) {
            setlocalState(localstorageKeys.adPageRedirectValidate, adPageRedirectInitialState)
        }
    }, [dispatch]);

    useEffect(() => {
        if(product) {
            setForm(product)
        }
    }, [product]);

    const setMainPhoto = (index) => {
        setMainPhotoIndex(index)
    }

    const updateMainPhotoIndexIfNeeded = (index, imagesLength) => {
        if (mainPhotoIndex === index && mainPhotoIndex !== 0) {
            setMainPhotoIndex(index - 1);
        }

        if (mainPhotoIndex >= imagesLength) {
            setMainPhotoIndex(imagesLength - 1);
        }
    };

    const onDeleteHandler = useCallback(async (index) => {
        const images = [...form.images];
        const deletedImage = images.splice(index, 1);
        setForm(prevState => ({...prevState, images: images}));

        updateMainPhotoIndexIfNeeded(index, images.length);

        await Api.put(buildRemoveImageApiRoute(form.uuid), {url: deletedImage[0]}).then(() => {})
    }, [form.images, mainPhotoIndex]);

    const onChangeImages = image => {
        const images = [...form.images];

        if (!(images.length < 1)) {
            const mainPhoto = images.splice(mainPhotoIndex, 1)
            images.unshift(mainPhoto[0], image);
        } else {
            images.unshift(image)
        }
        setForm(prevState => ({ ...prevState, images }));
        setMainPhotoIndex(0)
    };

    const onFieldChange = (fieldName: keyof IErrorsFormState) => {
        return (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
            setForm({ ...form, [fieldName]: e.target.value });
            setErrors({ ...errors, [fieldName]: e.target.value.trim() ? null : t('required_box') });
        }
    }

    return <React.Fragment>
        <ImageUploader
            uuid={form?.uuid}
            onAdd={onChangeImages}
            onDelete={onDeleteHandler}
            images={form?.images}
            setMainPhoto={setMainPhoto}
            mainPhoto={mainPhotoIndex}
        />
        <Input
            label={t('title')}
            value={form?.title}
            onChange={onFieldChange('title')}
            error={errors?.title}
            id='create__input'
        />
        <Textarea
            label={t('description')}
            rows={15}
            value={form?.description}
            onChange={onFieldChange('description')}
            count={form?.description?.length}
            error={errors?.description}
            id='create__textarea'
        />
    </React.Fragment>
};

export default AdDescription;
