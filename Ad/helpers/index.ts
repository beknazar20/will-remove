import { IAdDetails } from "../types";

export const transformAdDetailsTypeToFormType = (adDetails: IAdDetails, categories) => {
    const currentCategory = categories.find((category) => category.id === adDetails.category.parent?.id);
    return {
        category: currentCategory,
        subcategory: adDetails.category,
        ad_type: {
            title: adDetails.ad_type,
            id: adDetails.ad_type,
        },
    }
}